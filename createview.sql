CREATE VIEW ManageEmployee AS
SELECT EmployeeID , EmployeeName , EmployeeSurname , DeptName , ManagerName , ManagerSurname
FROM Employee LEFT JOIN Department ON (Employee.DeptID = Department.DeptID)
    LEFT JOIN Manager ON (Department.DeptID = Manager.DeptID)

