DROP TABLE IF EXISTS Employee;
DROP TABLE IF EXISTS Manager;
DROP TABLE IF EXISTS Department;

CREATE TABLE Department
(
	DeptID 		varchar(10)	not null,
	DeptName	varchar(30)	not null,
	PRIMARY KEY(DeptID)
) Engine=InnoDB;

CREATE TABLE Manager
(
	ManagerID	varchar(10)	not null,
	ManagerName	varchar(30)	not null,
	ManagerSurname	varchar(30)	not null,
	DeptID		varchar(10)	not null,
	PRIMARY KEY(ManagerID),
	FOREIGN KEY(DeptID) REFERENCES Department(DeptID)
) Engine=InnoDB;

CREATE TABLE Employee
(
	EmployeeID	varchar(10)	not null,
	EmployeeName	varchar(30)	not null,
	EmployeeSurname	varchar(30)	not null,
	DeptID		varchar(10)	not null,
	ManagerID	varchar(10)	not null,
	PRIMARY KEY(EmployeeID),
	FOREIGN KEY(DeptID) REFERENCES Department(DeptID),
	FOREIGN KEY(ManagerID) REFERENCES Manager(ManagerID)
) Engine=InnoDB;
